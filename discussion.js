/*
	Aggregation PipeLine Syntax:

		db.collectionName.aggregate([
			{Stage 1},
			{Stage 2},
			{Stage 3}
		]);

*/

/*
	$group is used to group elements/documents together & create an analysis of these grouped documents

	Syntax:
		{
			$group: {_id: <id>, fieldResult: "valueResult"}
		}

*/

// Count all the documents in our database
db.course_bookings.aggregate([
		{
			$group: {
				_id: null, count: {$sum: 1}
			}
		}
	]);

db.course_bookings.aggregate([
		{
			$group: {
				_id: "$studentId", count: {$sum: 1}
			}
		}
	]);

/*
	$match is used to pass the documnets or get the documents that will match our condition

	SYntax:
		{
			$match: {field: value}
		}

*/

db.course_bookings.aggregate([
		{
			$match: {
				"isCompleted": true
			}
		},

		{
			$group: {
				_id: "$courseId", total: {$sum: 1}
			}
		}
	]);

/*
	$project - this will alows us to show or hide the details

	SYntax:
		{
			$project: { field: 0 or 1 } // 0 - is to hide & 1 - is to show
		}
	
*/

db.course_bookings.aggregate([
		{
			$match: {
				"isCompleted": true
			}
		},

		{
			$project: {
				 "courseId": 0,
				 "studentId": 0
			}
		}
	]);

// used to count or to get the total sum of the documents
db.course_bookings.count();

/*
	$sort - can be used to changed the order of the aggregated results

	Syntax:
		{
			$sort: { field: 1/-1 } // 1 - is for asc order & -1 - is for desc order
		}
*/


db.course_bookings.aggregate([
		{
			$match: {
				"isCompleted": true
			}
		},

		{
			$sort: {
				"courseId": -1
			}
		}
	]);

// MINI ACTIVITY
/*
	1. Count the completed courses of student s013
*/

db.course_bookings.aggregate([
	{
		$match: {
			"isCompleted": true,
			"studentId": "s013"
		} 
	},
	{
		$group: {
			_id: null,
			counted_courses: {$sum: 1}
		}
	}
	])

// 2. Sort the courseID in descending order while studentId in ascending order.

db.course_bookings.aggregate([
	{
		$sort: {
			"courseId": -1,
			"studentId": 1
		}
	}

	])

db.orders.insertMany([
		{
			"cust_Id": "A123",
			"amount": 500,
			"status": "A"
		},
		{
			"cust_Id": "A123",
			"amount": 250,
			"status": "A"
		},
		{
			"cust_Id": "B212",
			"amount": 200,
			"status": "A"
		},
		{
			"cust_Id": "B212",
			"amount": 200,
			"status": "D"
		},

	])

db.orders.aggregate([
		{
			$match: {
				"status": "A"
			}
		},
		{
			$group: {
				_id: "$cust_Id", total: {$sum: "$amount"}
			}
		}

	]);

/*
	$sum - summation of all
	$avg - average
	$min - minimum
	$max - maximum
*/

db.orders. aggregate([
		{
			$match: {
				"status": {
					$in: ["A", "D"]
				}
			}
		},
		{
			$group: {
				_id: "$cust_Id", maxAmount: {$max: "$amount"}
			}
		}
	]);


